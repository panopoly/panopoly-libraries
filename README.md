Panopoly Libraries
==================

This project generates a Composer repository that contains libraries used by
the Panopoly project.

We originally relied on Asset Packgist for Javascript libraries that weren't
on Packagist, or were on Packagist, but wouldn't go into Drupal's `libraries/`
directory without configuration in your site's top-level composer.json.
However, in June 2022, Asset Packagist went offline for unknown reasons.

Since Panopoly is both a distribution, AND a set of modules that can all be
used independently, we don't want to have to constantly instruct users to make
various changes to their top-level composer.json as Panopoly's dependencies
evolve.

Instead, they simply need to add this Composer repository to their top-level
composer.json, and we will update the repository as needed.

You can browse the repository here:

https://panopoly.gitlab.io/panopoly-libraries/

To use these packages, add this to the "repositories" section of your
site's top-level composer.json, which you can do by hand or by running this
command:

    composer config repositories.panopoly-libraries composer https://panopoly.gitlab.io/panopoly-libraries

